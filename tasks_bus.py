from tasks import post_message, KEY_EXPIRES_IN, riak, logger
import redis
import urllib.request
import urllib.parse
import json
from celery.schedules import crontab
from celery import Celery
import celery_pubsub

# bus card number
#BUS_CARD_NUMBER = 'bus_card_number'
# icon to be displayed in bot message
ICON_BUS = b'\xF0\x9F\x9A\x8C'.decode('utf8')
# icon negative payment
ICON_NEGATIVE_BALANCE = b'\xE2\xAC\x87'.decode('utf8')
# icon positive balance
ICON_POSITIVE_BALANCE = b'\xE2\xAC\x86'.decode('utf8')
# balance cache
#balance_cache = redis.StrictRedis(host='localhost', port=6379, db=2)
bus_riak = riak.bucket('tasks_bus')
BUS_CARD_NUMBER = '03332040332'



app = Celery('bus', broker='amqp://slonnik:Deda19770802@localhost:5672/myvhost')

app.conf.beat_schedule = {
   'bus-update': {
        'task': 'tasks_bus.bus_query_for_update',
        'schedule': crontab(minute='*/1')
    }
}



app.conf.task_routes = {'tasks_bus.*': {'queue': 'bus'}}

@app.task
def bus_command(*args, **kwargs):
    logger.error('bus command arrive')

celery_pubsub.subscribe('command', bus_command)

@app.task
def bus_query():
    balance = bus_query_for_balance(BUS_CARD_NUMBER)
    if balance is not None:
        store_balance(card_number=BUS_CARD_NUMBER, balance=balance)
        post_message(text="{} {}".format(ICON_BUS, balance / 100.0))

@app.task
def bus_query_for_update():
    new_balance = bus_query_for_balance(BUS_CARD_NUMBER)
    if new_balance is not None:
        balance = get_balance(card_number=BUS_CARD_NUMBER)
        if balance is None:
            balance = 0.0
        balance = float(balance)
        if balance != new_balance:
            payment = (new_balance - balance)/100.0
            sign = ICON_NEGATIVE_BALANCE
            if payment > 0:
                sign = ICON_POSITIVE_BALANCE
            text = '{} {} {}{}'.format(ICON_BUS, new_balance/100.0, sign, abs(payment))
            post_message(text=text)
            store_balance(card_number=BUS_CARD_NUMBER, balance=new_balance)

def bus_query_for_balance(card_id):
    if card_id is None:
        return None
    url = 'http://strelkacard.ru/api/cards/status/?cardnum={}&cardtypeid=3ae427a1-0f17-4524-acb1-a3f50090a8f3'.format(
        card_id)
    data = json.loads(urllib.request.urlopen(url).read().decode('utf8'))
    if 'balance' in data:
        return float(data['balance'])
    return None

def store_balance(card_number, balance):
    bus_riak.new(card_number, data=balance).store()


def get_balance(card_number):
    fetched = bus_riak.get(card_number)
    if fetched.exists:
        return fetched.data
    return None