from tasks import post_message, logger, KEY_EXPIRES_IN
import httplib2
import io
import os
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import redis
from celery.schedules import crontab
from celery import Celery
import base64

app = Celery('gmail', broker='amqp://slonnik:Deda19770802@localhost:5672/myvhost')

app.conf.beat_schedule = {
    'gmail-update': {
        'task': 'tasks_gmail.gmail_query_inbox',
        'schedule': crontab(minute='*/5')
    }
}

app.conf.task_routes = {'tasks_gmail.*': {'queue': 'gmail'}}

flags = None

# message icon to be displayed in bot message
ICON_MESSAGE = b'\xE2\x9C\x89'.decode('utf8')

# gmail messages cache
gmail_msg_ids = redis.StrictRedis(host='localhost', port=6379, db=3)


# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/gmail-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Python Quickstart'
def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'gmail-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

@app.task
def command():
    logger.error('gmail command arrive')

@app.task
def gmail_query_inbox():
    """Shows basic usage of the Gmail API.

    Creates a Gmail API service object and outputs a list of label names
    of the user's Gmail account.
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    results = service.users().messages().list(userId='me',q='is:unread is:inbox').execute()
    messages = results.get('messages', [])
    if not messages:
        print('No messages found.')
    else:
        for message in messages:
            message_ = service.users().messages().get(userId='me', id=message['id']).execute()
            for header in message_['payload']['headers']:
                if 'From' == header['name']:
                    from_ = header['value']
                    if from_ is not None:
                        from_ = from_.split('<')[0]
                        from_ = from_.rstrip()
                        from_ = from_.rstrip('<>')
                if 'Subject' == header['name']:
                    subject_ = header['value']
            # send message
            labels = {'removeLabelIds': ['UNREAD'], 'addLabelIds': []}
            service.users().messages().modify(userId='me', id=message['id'], body=labels).execute()
            post_id = post_message(text="{} {}  {}".format(ICON_MESSAGE, from_, subject_))
            if post_id is not None:
                gmail_msg_ids.set(post_id, message['id'], KEY_EXPIRES_IN)

            # send attachments
            try:
                for part in message_['payload']['parts']:
                    file_name = part['filename']
                    logger.info('Attachment file: {}'.format(file_name))
                    if file_name:
                        attachment = service.users().messages().attachments().\
                            get(userId='me', messageId=message['id'], id=part['body']['attachmentId']).execute()
                        file_data = base64.urlsafe_b64decode(attachment['data'].encode('UTF-8'))
                        post_id = post_message(text="{} {}".format(ICON_MESSAGE, file_name), file_name=file_name, data=io.BytesIO(file_data))
                        if post_id is not None:
                            gmail_msg_ids.set(post_id, message['id'], KEY_EXPIRES_IN)
            except Exception as ex:
                logger.error("Failed to save attachment due to {}".format(ex))

@app.task
def gmail_delete_message(reply_to_id):
    """
    Delete Gmail message related to reply_to_id
    :param reply_to_id:
    :return:
    """
    gmail_message_id = gmail_msg_ids.get(reply_to_id)
    if gmail_message_id is None:
        logger.info('No meail found')
        return
    gmail_message_id = gmail_message_id.decode('utf8')
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    service.users().messages().delete(userId='me', id=gmail_message_id).execute()
