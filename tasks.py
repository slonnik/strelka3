from __future__ import print_function

import telegram
import ssl
from celery.utils.log import get_task_logger
import redis
import riak
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram import KeyboardButton, ReplyKeyboardMarkup

# chat id key
CHAT_ID = 'chat_id'
# KEY EXPIRES IN ... (1 month)
KEY_EXPIRES_IN = 60*60*24*365
# path to file store
FILE_STORE_PATH = '/strelka/STORE/'


global config_map
config_map= {}

global bot
bot = telegram.Bot(token='535783150:AAHTvl4YMsw4dXKiBVc4MjtEbzws8pL86eM')
config_db = redis.StrictRedis(host='localhost', port=6379, db=4)
logger = get_task_logger(__name__)
ssl._create_default_https_context = ssl._create_unverified_context


riak = riak.RiakClient(pb_port=8087)
config_riak = riak.bucket('config')

def post_message(text, file_name=None, data=None, action="Delete"):
    chat_id = get_config_value(CHAT_ID)
    if chat_id is None:
        return None
    if file_name is None:
        keyboard = [[InlineKeyboardButton(action, callback_data=action)]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        telegram_message = bot.send_message(chat_id=chat_id, text=text, reply_markup=reply_markup)
        return telegram_message.message_id
    else:
        telegram_message = bot.send_document(chat_id=chat_id,  filename=file_name, document=data, caption=text)
        return telegram_message.message_id

def post_message_ex(text):
    chat_id = get_config_value(CHAT_ID)
    if chat_id is None:
        return None

    keyboard = [[KeyboardButton("Tasks", callback_data="tasks_live.live_get_tasks"),
                 KeyboardButton("Bus", callback_data="tasks_bus.bus_query")]]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard = True)
    telegram_message = bot.send_message(chat_id=chat_id, text=text, reply_markup=reply_markup)
    return telegram_message.message_id

def delete_message(message_id):
    chat_id = get_config_value(CHAT_ID)
    if chat_id is None:
        return None
    bot.delete_message(chat_id=chat_id, message_id = message_id)

def get_config_value(key):
    fetched = config_riak.get(key)
    if fetched.exists:
        return fetched.data
    return None

def set_config_value(key, value):
    config_riak.new(key, data=value).store()
