from celery import Celery
from celery.schedules import crontab
from tasks import post_message
import psutil
import os

app = Celery('watchdog', broker='redis://localhost:6379/11')

app.conf.beat_schedule = {
   'watch-update': {
        'task': 'tasks_watchdog.check_for_web_server',
        'schedule': crontab(minute='*/5')
    }
}

@app.task
def check_for_web_server():
    for pid in psutil.pids():
        p = psutil.Process(pid)
        for line in p.cmdline():
            if 'main' in line:
                return
    os.system('/strelka/bin/python /strelka/main.py')
    post_message('restart web service')
