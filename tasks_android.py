from celery import Celery
from tasks import post_message
app = Celery('android', broker='redis://localhost:6379/11')

ICON_MOBILE_PHONE = b'\xF0\x9F\x93\xB2'.decode('utf8')

@app.task
def android_message(text):
    if text is not None and len(text.strip()) > 0:
        post_message('{} {}'.format(ICON_MOBILE_PHONE, text))
