from tasks import CHAT_ID, delete_message, set_config_value, post_message_ex
from tasks_live import live_login
from tasks_android import android_message
import telegram
import ssl
import flask
from flask import Flask, request
import uuid
from flask_oauthlib.client import OAuth
import config
from flask import logging
import requests
import json
import collections
from celery import Celery
import celery_pubsub


celery_app = Celery('web', broker='amqp://slonnik:Deda19770802@localhost:5672/myvhost')
celery_app.conf.task_routes = {'tasks_bus.*': {'queue': 'bus'}, 'tasks_live.*': {'queue': 'live'},
                               'tasks_gmail.*': {'queue': 'gmail'}}


class SimpleTask:
    def __init__(self, func):
        self.func = func
    def __call__(self, *args, **kwargs):
        self.func(*args, **kwargs)

class CeleryTask:
    def __init__(self, app, name):
        self.name = name
        self.app = app

    def __call__(self, *args, **kwargs):
        celery_app.send_task(self.name, kwargs=kwargs)

class CallBacksDict(collections.UserDict):
    def __getitem__(self, update):
        if hasattr(update, 'callback_query') and hasattr(update.callback_query, 'data'):
            return collections.UserDict.__getitem__(self, update.callback_query.data)
        self.__missing__(update)

    def __contains__(self, update):
        if hasattr(update, 'callback_query') and hasattr(update.callback_query, 'data'):
            return collections.UserDict.__contains__(self, update.callback_query.data)
        return False

    def __missing__(self, key):
        return ['main.foo_method']

class CommandsDict(collections.UserDict):
    def __getitem__(self, update):
        if not update.message:
            self.__missing__(update)
        return collections.UserDict.__getitem__(self, update.message.text.lower())

    def __contains__(self, update):
        if not update.message:
            return False
        return collections.UserDict.__contains__(self, update.message.text.lower())

    def __missing__(self, key):
        return 'main.post_bot_message_ex'

def delete_bot_message(*args, **kwargs):
    delete_message(kwargs['reply_to_id'])

callbacks = CallBacksDict({'Delete': [CeleryTask(celery_app, 'tasks_live.live_delete_message'),
                                      CeleryTask(celery_app, 'tasks_gmail.gmail_delete_message'),
                                      SimpleTask(delete_bot_message)],
             'Complete': [CeleryTask(celery_app,'tasks_live.live_complete_task'), SimpleTask(delete_bot_message)]})
commands = CommandsDict({'bus': CeleryTask(celery_app,'tasks_bus.bus_query'), 'tasks': CeleryTask(celery_app,'tasks_live.live_get_tasks')})




logger = logging.getLogger()
global bot
bot = telegram.Bot(token='535783150:AAHTvl4YMsw4dXKiBVc4MjtEbzws8pL86eM')

app = Flask(__name__, template_folder='static/templates')
port = 443

app.debug = True
app.secret_key = 'development'
OAUTH = OAuth(app)
MSGRAPH = OAUTH.remote_app(
    'microsoft', consumer_key=config.CLIENT_ID, consumer_secret=config.CLIENT_SECRET,
    request_token_params={'scope': config.SCOPES},
    base_url=config.RESOURCE + config.API_VERSION + '/',
    request_token_url=None, access_token_method='POST',
    access_token_url=config.AUTHORITY_URL + config.TOKEN_ENDPOINT,
    authorize_url=config.AUTHORITY_URL + config.AUTH_ENDPOINT)


@celery_app.task
def foo_method():
    pass


@app.route('/HOOK', methods=['POST'])
def webhook_handler():
    update = telegram.Update.de_json(request.get_json(force=True), bot)
    if not update:
        return # no update arrived

    # update chat id
    if update.message:
        set_config_value(CHAT_ID, update.message.chat_id)
    # handle update
    if update in callbacks:
        # handle call back response
        for callback in callbacks[update]:
            callback(reply_to_id=update.callback_query.message.message_id)
    elif update in commands:
        # handle command
        command = commands.get(update)
        command()
    else:
        post_message_ex('test')

    return 'ok'

@app.route('/set_webhook', methods=['GET', 'POST'])
def set_webhook():
    s = bot.setWebhook("https://slonnik.ru:{}/HOOK".format(port))
    if s:
        return "webhook setup ok"
    else:
        return "webhook setup failed"

@app.route('/delete_webhook', methods=['GET', 'POST'])
def delete_webhook():
    s = bot.deleteWebhook()
    if s:
        return "webhook delete ok"
    else:
        return "webhook delete failed"


@app.route('/gmail_hook', methods=['GET', 'POST'])
def gmail_hook():
    return 'ok'

@app.route('/')
def index():
    return 'ok'


@app.route('/office365_hook', methods=['GET', 'POST'])
def office365_hook():
    """Handler for the application's Redirect Uri."""
    if str(flask.session['state']) != str(flask.request.args['state']):
        raise Exception('state returned to redirect URL does not match!')
    code = flask.request.args['code']
    payload = 'client_id={}&scope=User.Read%20Mail.ReadWrite%20Tasks.ReadWrite%20offline_access&code={}&redirect_uri={}&grant_type=authorization_code&client_secret={}'.format(config.CLIENT_ID, code, config.REDIRECT_URI, config.CLIENT_SECRET)
    response = requests.post("https://login.microsoftonline.com/common/oauth2/v2.0/token", data=payload)
    content = json.loads(response.content.decode('utf8'))
    access_token = content.get('access_token')
    refresh_token = content.get('refresh_token')
    expires_in = content.get('expires_in')
    if access_token is None:
        logger.info('Response: ' + response.content.decode('utf8'))
    else:
        logger.info('Access_token: ' + access_token)	
    live_login.delay(access_token, refresh_token, expires_in)
    return '200'

@app.route('/office365_mail_hook', methods=['GET', 'POST'])
def office365_mail_hook():
    return '200'

@app.route('/android_hook', methods=['GET', 'POST'])
def android_hook():
    #data = json.load(request.data.decode('utf8'))
    android_message.delay(request.args.get('text'))
    return '200'

@app.route('/login')
def login():
    """Prompt user to authenticate."""
    flask.session['state'] = str(uuid.uuid4())
    return MSGRAPH.authorize(callback=config.REDIRECT_URI, state=flask.session['state'])

if __name__ == "__main__":
    ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    ctx.load_cert_chain('/strelka/cert/strelka.pem', '/strelka/cert/strelka.key')
    app.run(host='0.0.0.0', port=port, ssl_context=ctx)
