from tasks import logger, post_message, get_config_value, set_config_value, KEY_EXPIRES_IN

from datetime import datetime
from datetime import timedelta
import redis
import json
import requests
import config
from celery.schedules import crontab
from celery import Celery
import base64
import io

app = Celery('live', broker='amqp://slonnik:Deda19770802@localhost:5672/myvhost')

app.conf.beat_schedule = {
     'live-update': {
        'task': 'tasks_live.live_query_inbox',
        'schedule': crontab(minute='*/5')
    }
}

app.conf.task_routes = {'tasks_live.*': {'queue': 'live'}}

# MSGRAPH access token
LIVE_ACCESS_TOKEN = 'live_access_token'
# MSGRAPH refresh token
LIVE_REFRESH_TOKEN = 'live_refresh_token'
# MSGRAPH expires token
LIVE_EXPIRES_IN_TOKEN = 'live_expires_in'
# MSGRAPH scope
LIVE_SCOPE = 'User.Read%20Mail.ReadWrite%20Tasks.ReadWrite%20offline_access'
# email icon to be displayed in bot message
ICON_MESSAGE = b'\xF0\x9F\x93\xA7'.decode('utf8')
# task icon to be displayed in bot message
ICON_TASK = b'\xE2\x9C\x85'.decode('utf8')
ICON_TASK_CLOCK =b'\xE2\x8F\xB0'.decode('utf8')

# root MSGRAPH API URI
URI_ROOT = 'https://graph.microsoft.com/v1.0/me'
# outlook inbox query API
URI_QUERY_INBOX = 'mailFolders/inbox/messages?$filter=isRead eq false'
# uri to update message
URI_UPDATE_MESSAGE = 'messages'
# MSGRAPH Auth
URI_AUTH_TOKEN = 'https://login.microsoftonline.com/common/oauth2/v2.0/token'
# MSGRAPH TASKS ROOT FOLDER
URI_TASK_FOLDER = 'https://graph.microsoft.com/beta/me/outlook/taskFolders'
# MSGRAPH Query tasks
URI_QUERY_TASKS = 'https://graph.microsoft.com/beta/me/outlook/taskFolders/{}/tasks'
# MSGRAPH Update task
URI_UPDATE_TASK = 'https://graph.microsoft.com/beta/me/outlook/tasks'
# outlook messages cache
live_msg_ids = redis.StrictRedis(host='localhost', port=6379, db=5)
# outlook tasks cache
live_task_ids = redis.StrictRedis(host='localhost', port=6379, db=6)

@app.task
def command():
    logger.error('live command arrive')

@app.task
def live_login(access_token, refresh_token, expires_in):
    """
    Login into live service
    :param access_token:
    :param refresh_token:
    :param expires_in:
    :return:
    """
    set_config_value(key=LIVE_ACCESS_TOKEN, value=access_token)
    set_config_value(key=LIVE_REFRESH_TOKEN, value=refresh_token)
    set_config_value(key=LIVE_EXPIRES_IN_TOKEN, value=expires_in)
    # periodically refresh token
    live_refresh_token.apply_async(eta=datetime.now() + timedelta(seconds=int(get_config_value(key=LIVE_EXPIRES_IN_TOKEN))))

@app.task
def live_query_inbox():
    """
    Query outlook inbox
    :return:
    """
    access_token=get_config_value(key=LIVE_ACCESS_TOKEN)
    if access_token: # we found access tocken
        # query for inbox messages
        response = requests.get('{}/{}'.format(URI_ROOT, URI_QUERY_INBOX), headers={'Authorization': access_token})
        messages = json.loads(response.content.decode('utf8')).get('value')
        if not messages:
            # no messages found in inbox
            logger.info(response.content)
            return

        for message in messages:
            # mark email as 'read'
            requests.patch('{}/{}/{}'.format(URI_ROOT,URI_UPDATE_MESSAGE, message.get('id')),
                           data=json.dumps({'IsRead': True}),
                           headers={'Authorization': access_token, 'Content-type': 'application/json'})

            # outgoing message content
            content = (ICON_MESSAGE, message.get('from').get('emailAddress').get('name'), message.get('subject'))
            # post message to bot
            post_id = post_message(text="%s %s %s" % content)
            if post_id: # posted message successfully
                live_msg_ids.set(post_id, message['id'], KEY_EXPIRES_IN) # store id in cache
            # check for attachment
            has_attachment = message.get('hasAttachments')
            if has_attachment:
                response = requests.get('{}/{}/{}/attachments'.format(URI_ROOT, URI_UPDATE_MESSAGE, message.get('id')),
                               headers={'Authorization': access_token})
                attachments = json.loads(response.content.decode('utf8')).get('value')
                for attachment in attachments:
                    file_name = attachment.get('name')
                    file_data = base64.urlsafe_b64decode(attachment.get('contentBytes').encode('UTF-8'))
                    # post attachment
                    post_message(text=file_name, file_name=file_name, data=io.BytesIO(file_data))

@app.task
def live_delete_message(reply_to_id):
    """
    Deelete outlook message related to reply_to_id
    :param reply_to_id:
    :return:
    """
    if reply_to_id not in live_msg_ids:
        return # now live id message with reply_to_id
    access_token = get_config_value(key=LIVE_ACCESS_TOKEN)
    if access_token: # we have access tocken
        message_id = live_msg_ids.get(reply_to_id).decode('utf8') # find message in the cache of sent messages
        if message_id:
            # send request to delete message
            requests.delete("{}/{}/{}".format(URI_ROOT, URI_UPDATE_MESSAGE, message_id),
                    headers={'Authorization': access_token})

@app.task
def live_refresh_token():
    """
    Refresh token
    :return:
    """
    refresh_token = get_config_value(key=LIVE_REFRESH_TOKEN) #find refresh token
    if refresh_token:
        # send request to refresh tocken
        payload = 'client_id={}&scope={}&refresh_token={}&redirect_uri={}&grant_type=refresh_token&client_secret={}'.format(
            config.CLIENT_ID, LIVE_SCOPE, refresh_token, config.REDIRECT_URI, config.CLIENT_SECRET)
        response = requests.post(URI_AUTH_TOKEN, data=payload)
        content = json.loads(response.content.decode('utf8'))
        access_token = content.get('access_token')
        refresh_token = content.get('refresh_token')
        expires_in = content.get('expires_in')
        # save new credentials
        live_login(access_token, refresh_token, expires_in)

@app.task
def live_get_tasks():
    """
    Get outlook task
    :return:
    """
    # get access tocken
    access_token = get_config_value(key=LIVE_ACCESS_TOKEN)
    if not access_token:
        logger.info('Missing token') #no tocken found
    else:
        # call for task root folder
        response = requests.get(URI_TASK_FOLDER, headers={'Authorization': access_token})
        roots = json.loads(response.content.decode('utf8')).get('value') # found root folder for tasks
        if not roots or len(roots) == 0:
            logger.info('No root folder')
            logger.info('Access token: ' + access_token)
            logger.info(response.content.decode('utf8'))
            return
        for root in roots:
            root_id = root.get('id')        # task subfolder id
            root_name = root.get('name')    # task subfolder name
            # query for subfolder
            response = requests.get(URI_QUERY_TASKS.format(root_id), headers={'Authorization': access_token})
            # tasks in subfolder
            tasks = json.loads(response.content.decode('utf8')).get('value')
            if not tasks:
                logger.info(response.content.decode('utf8'))
                return
            for task in tasks:
                if 'completed' != task.get('status'):
                    title = task.get('subject')
                    due_date = task.get('dueDateTime')
                    if due_date:
                        due_date = due_date.get('dateTime')
                        due_date = datetime.strptime(due_date, '%Y-%m-%dT%H:%M:%S.0000000')
                    else:
                        due_date = datetime.now()
                    due_date = datetime.strftime(due_date, '%d %b, %a')
                    post_id = post_message(text="{} {}: {} {} {}".format(ICON_TASK, root_name, title, ICON_TASK_CLOCK, due_date), action='Complete')
                    if post_id:
                        # succeeded with getting task - store it in cache
                        live_task_ids.set(post_id, task.get('id'), KEY_EXPIRES_IN)
@app.task
def live_complete_task(reply_to_id):
    """
    Set complete status to task
    :param reply_to_id:
    :return:
    """
    access_token = get_config_value(LIVE_ACCESS_TOKEN) # get access tocken
    if access_token:
        tasks_id = live_task_ids.get(reply_to_id) # get task from cache
        if tasks_id:
            tasks_id = tasks_id.decode('utf8')
            # send request to comple tasks
            response = requests.patch("{}('{}')".format(URI_UPDATE_TASK, tasks_id),
                   headers={'Authorization': access_token, 'Content-type': 'application/json'}, data=json.dumps({'status': 'completed'}))
            logger.info(response.content)
